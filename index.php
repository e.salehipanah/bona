<?php get_header(); // get_header: load header ?>

<!-- <div class="slider"></div> -->
<!-- slider -->

	<section class="blog-area section">
		<div class="container">
			<div class="row">
			<?php
			if(have_posts()){
				while(have_posts()){
					the_post();
					?>
						<div class="col-lg-4 col-md-6">
							<div class="card h-100">
								<div class="single-post post-style-1">

									<div class="blog-image"><img src="<?php echo (get_the_post_thumbnail_url() != '' ? get_the_post_thumbnail_url() : dir . 'assets/no-image-found.jpg'); ?>" alt="<?php the_title(); ?>"></div>

									<a class="avatar" href="#"><img src="<?php echo get_avatar_url(get_the_author_id()) ?>" alt="<?php echo get_the_author_meta( 'display_name' ) ?>"></a>

									<div class="blog-info">

										<h4 class="title"><a href="<?php the_permalink() ?>"><b><?php the_title() ?></b></a></h4>

										<ul class="post-footer">
											<li><a href="<?php the_permalink() ?>">More</a></li>
											<li><a href="#"><i class="ion-chatbubble"></i><?php echo get_comments_number(get_the_ID()) ?></a></li>
											<li><a href="#"><i class="ion-eye"></i><?php echo getPostViews(get_the_ID()); ?></a></li>
										</ul>

									</div><!-- blog-info -->
								</div><!-- single-post -->
							</div><!-- card -->
						</div><!-- col-lg-4 col-md-6 -->
					<?php
				}
				
			}else{
				?>
				Nothing . . .
				<?php
			}
			?>
			</div><!-- row -->

		</div><!-- container -->
	</section><!-- section -->


<?php get_footer();?>

	
