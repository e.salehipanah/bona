<?php get_header();the_post(); setPostViews(get_the_ID()) ?>
	<link href="<?php echo dir;?>single-post-3/css/styles.css" rel="stylesheet">

	<link href="<?php echo dir;?>single-post-3/css/responsive.css" rel="stylesheet">

	<style media="screen">
		.slider{
			background-image: url('<?php the_post_thumbnail_url(); ?>') !important;
		}
	</style>
	<div class="slider center-text">
		<div class="container height-100">
			<div class="row height-100">

				<div class="col-lg-1 hidden-md-down"></div>

				<div class="col-lg-10 col-md-12">
					<div class="display-table">
						<div class="display-table-cell">
							<h5 class="pre-title"><?php $category_detail=get_the_category();//$post->ID
								foreach($category_detail as $cd){
								echo $cd->cat_name;
								} ?></h5>

							<h3 class="title"><a href="#"><b><?php the_title(); ?></b></a></h3>

							<div class="post-info">

								<div class="left-area">
									<a class="avatar" href="#"><img src="<?php echo get_avatar_url(get_the_author_id()) ?>" alt="Profile Image"></a>
								</div>

								<div class="middle-area">
									<a class="name" href="#"><b><?php the_author_meta('display_name') ?></b></a>
									<h6 class="date">on <?php the_date() ?></h6>
								</div>

							</div><!-- post-info -->

						</div><!-- display-table-cell -->
					</div><!-- display-table -->

				</div><!-- col-lg-10 -->
			</div><!-- row -->
		</div><!-- conatiner -->
	</div><!-- slider -->

	<section class="post-area">
		<div class="container">
			<div class="row">

				<div class="col-md-1"></div>
				<div class="col-md-10">

					<div class="main-post">

						<div class="post-top-area">

							<p class="para"><?php the_content(); ?></p>

						</div><!-- post-top-area -->

					</div><!-- main-post -->
				</div><!-- col-lg-8 -->

			</div><!-- row -->
		</div><!-- container -->

		

		<div class="container">
			<div class="row">

				<div class="col-md-1"></div>
				<div class="col-md-10">

					<div class="main-post">

						<div class="post-bottom-area">
						
							
						<ul class="tags">
						<?php the_tags( '<li>', '</li><li>', '</li>' ); ?>
			
							</ul>

							<div class="post-icons-area">
								<ul class="post-icons">
									<li><a href="#"><i class="ion-chatbubble"></i><?php echo get_comments_number(get_the_ID()) ?></a></li>
									<li><a href="#"><i class="ion-eye"></i><?php echo getPostViews(get_the_ID()); ?></a></li>
								</ul>

								<ul class="icons">
									<li>SHARE : </li>
									<li><a href="#"><i class="ion-social-facebook"></i></a></li>
									<li><a href="#"><i class="ion-social-twitter"></i></a></li>
									<li><a href="#"><i class="ion-social-pinterest"></i></a></li>
								</ul>
							</div>

							<div class="post-footer post-info">

								<div class="left-area">
								<a class="avatar" href="#"><img src="<?php echo get_avatar_url(get_the_author_id()) ?>" alt="Profile Image"></a>
								</div>

								<div class="middle-area">
									<a class="name" href="#"><b><?php the_author_meta('display_name') ?></b></a>
									<h6 class="date">on <?php echo get_the_date() ?></h6>
								</div>

							</div><!-- post-info -->

						</div><!-- post-bottom-area -->

					</div><!-- main-post -->
				</div><!-- col-md-10 -->
			</div><!-- row -->
		</div><!-- container -->

	</section><!-- post-area -->



	

	<?php get_footer(); ?>