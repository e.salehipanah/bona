<?php
/**
 * dir : theme file direction
 * define : global dir value 
 * the_function echo nemikhad
 */
$dir = get_template_directory_uri() . '/';
define('dir',$dir);
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
	<title><?php if(is_home()? get_bloginfo('title'):wp_title(' ')) ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">


	<!-- Font -->

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">


	<!-- Stylesheets -->

	<link href="<?php echo dir; ?>css/bootstrap.css" rel="stylesheet"> 

	<link href="<?php echo dir; ?>css/ionicons.css" rel="stylesheet">

	<link href="<?php echo dir; ?>layout-1/css/styles.css" rel="stylesheet">

	<link href="<?php echo dir; ?>layout-1/css/responsive.css" rel="stylesheet">

	<?php wp_head();//لود اون نوار بالای تم که برای ورد پرس هست  ?>
</head>
<body >
<header>
		<div class="container-fluid position-relative no-side-padding">

			<a href="<?php echo get_home_url() ?>" class="logo"><img src="<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
echo $image[0]; ?>" alt="<?php bloginfo('title') ?>"></a>

			<div class="menu-nav-icon" data-nav-menu="#main-menu"><i class="ion-navicon"></i></div>

			<!-- <ul class="main-menu visible-on-click" id="main-menu"> -->
			<?php wp_nav_menu(
				array(
					'theme_location' => 'header-menu',
					'menu_id'        => 'headr-menu',
					'menu_class'     => 'main-menu visible-on-click'
				)
			); ?>

			<div class="src-area">
				<form action="<?php bloginfo('url'); ?>" method="get"><!-- به تور پیش فرض باید آدرس صفحه اصلی وارد شود و متد گت -->
					<button class="src-btn" type="submit"><i class="ion-ios-search-strong"></i></button>
					<input class="src-input" type="text" name="s" placeholder="Type of search"><!-- بخش اینپوت سرچ به طور پیشفرض باید نام s داشته باشد -->
				</form>
			</div>

		</div><!-- conatiner -->
	</header>